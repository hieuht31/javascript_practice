// 1. COMPLETE VARIABLE AND FUNCTION DEFINITIONS

const customName = document.getElementById('customname');
const randomize = document.querySelector('.randomize');
const story = document.querySelector('.story');

function randomValueFromArray(array){
    // get random number from 0 -> array.length - 1 
  const random = Math.floor(Math.random()*array.length);
  return array[random];
}

/**
 * 
 * 
 * 
It was 94 fahrenheit outside, so :insertx: went for a walk. 
When they got to :inserty:, they stared in horror for a few moments, 
then :insertz:. Bob saw the whole thing, but was not surprised — :insertx: 
weighs 300 pounds, and it was a hot day.

Willy the Goblin
Big Daddy
Father Christmas

the soup kitchen
Disneyland
the White House

spontaneously combusted
melted into a puddle on the sidewalk
turned into a slug and crawled away

 * 
*/


randomize.addEventListener('click', result);

const insertX = ['Willy the Goblin','Big Daddy','Father Christmas']
const insertY = ['the soup kitchen','Disneyland','the White House']
const insertZ = ['spontaneously combusted','melted into a puddle on the sidewalk',
'turned into a slug and crawled away']


function result() {
    let name = 'Bob'
    let weight = 300
    let  temperature = 94
  if(customName.value !== '') {
    name = customName.value;
  }

  if(document.getElementById("uk").checked) {
    weight = Math.round(300);
    temperature =  Math.round(94);
  }

  story.textContent = `It was ${weight} fahrenheit outside, so ${randomValueFromArray(insertX)} went for a walk. 
  When they got to ${randomValueFromArray(insertY)}, they stared in horror for a few moments, 
  then ${randomValueFromArray(insertZ)}. ${name} saw the whole thing, but was not surprised — ${randomValueFromArray(insertX)} 
  weighs ${temperature} pounds, and it was a hot day.`;
  story.style.visibility = 'visible';
}